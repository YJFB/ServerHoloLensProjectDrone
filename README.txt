This folder contains the source the basis of the server which will share the drones'
trajectories with the HoloLens.

/!\ It is not yet functional and the /!\

getcoordinates.py
When launched it will gather the trajectories from the swarm and store them in a txt file


sendcoordinates.py
Uses the coordinates stored in the txt file and sends them to the HoloLens through a WebSocket

/!\ The final steps needed for the programs to work are described in their code /!\

