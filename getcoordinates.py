#!/usr/bin/env python2
# -*- coding: utf-8 -*-

'''
This programs connects to a drone (sitl for the moment) and dynamically updates a txt
file with the trajectory of the drone (GPS coordinates) 
Has to be adapted to multiple drones
'''
from dronekit import connect, Command
from pymavlink import mavutil
from random import randint
import time

#Connection to a a drone using tcp
vehicle = connect('tcp:localhost:5760', wait_ready=True, heartbeat_timeout=30, baud=115200)

wp=[]
cmds = vehicle.commands
cmds.clear()


while True:
#This part sends a new trajectory to the drone   
    time.sleep(1)
    cmds.clear()    
    for i in range(10):
        a,b=-35+randint(0,50)/10.,149+randint(0,50)/10.
        cmd = Command(0,0,0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0,a, b, 30)
        cmds.add(cmd)
        wp+=(a,b,30)
    cmds.upload()
    
#This part collects the trajectory of the drone and stores it in a txt file    
    cmds.download()
    with open("trajre.txt", "w") as f:
        for p in cmds:
            f.write(str(p.x)+"\n" + str(p.y)+"\n" + str(p.z)+"\n")
        print (cmds[0].x, cmds[0].y, cmds[0].z)
    
